﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class starSystem : MonoBehaviour
{
    public static int countStar;
    public Image star1;
    public Image star2;
    public Image star3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (countStar == 3)
        {
            star1.color = new Color(255, 255, 0f, 255);
            star2.color = new Color(255, 255, 0f, 255);
            star3.color = new Color(255, 255, 0f, 255);
        }
        if (countStar == 1)
        {
            star1.color = new Color(255, 255, 0f, 255);
        }
        if (countStar == 2)
        {
            star1.color = new Color(255, 255, 0f, 255);
            star2.color = new Color(255, 255, 0f, 255);
        }
    }
}
