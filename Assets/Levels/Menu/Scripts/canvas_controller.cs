﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class canvas_controller : MonoBehaviour
{
    public Canvas canvas_main;
    public Canvas canvas_levels;
    public Canvas canvas_setting;
    public Canvas canvas_control;
    public GameObject setting_logo;
    public GameObject levels_change_logo;
    public GameObject swichPanel;
    public Text textSwich;
    bool key = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void startButton()
    {
        canvas_main.GetComponent<Canvas>().enabled = false;
        canvas_levels.GetComponent<Canvas>().enabled = true;
        canvas_setting.GetComponent<Canvas>().enabled = false;
        canvas_control.GetComponent<Canvas>().enabled = false;
        levels_change_logo.GetComponent<Animator>().enabled = true;
    }

    public void settingButton()
    {
        canvas_main.GetComponent<Canvas>().enabled = false;
        canvas_levels.GetComponent<Canvas>().enabled = false;
        canvas_control.GetComponent<Canvas>().enabled = false;
        canvas_setting.GetComponent<Canvas>().enabled = true;
        setting_logo.GetComponent<Animator>().enabled = true;
    }

    public void parentControlButton()
    {
        canvas_main.GetComponent<Canvas>().enabled = false;
        canvas_levels.GetComponent<Canvas>().enabled = false;
        canvas_setting.GetComponent<Canvas>().enabled = false;
        setting_logo.GetComponent<Animator>().enabled = false;
        canvas_control.GetComponent<Canvas>().enabled = true;
    }
    public void swichButtonControl()
    {
        if (key==false)
        {
            swichPanel.GetComponent<Image>().enabled = false;
            key = true;
            textSwich.text = "Выключить";
        }
        else
        {
            swichPanel.GetComponent<Image>().enabled = true;
            key = false;
            textSwich.text = "Включить";
        }
    }

    public void backButton()
    {
        canvas_main.GetComponent<Canvas>().enabled = true;
        canvas_levels.GetComponent<Canvas>().enabled = false;
        canvas_setting.GetComponent<Canvas>().enabled = false;
        canvas_control.GetComponent<Canvas>().enabled = false;
    }

    public void backButtonControl()
    {
        canvas_main.GetComponent<Canvas>().enabled = false;
        canvas_levels.GetComponent<Canvas>().enabled = false;
        canvas_setting.GetComponent<Canvas>().enabled = true;
        canvas_control.GetComponent<Canvas>().enabled = false; 
    }
    
}
