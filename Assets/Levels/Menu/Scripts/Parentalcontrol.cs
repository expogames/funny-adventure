﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class Parentalcontrol : MonoBehaviour {

    public InputField inputData;

    void Start()
    {

    }
    private void Update()
    {
        
    }
    private IEnumerator Wait(float time)
    {
        yield return new WaitForSeconds(1f); // таймер, через 10 секунд
        time = time - 1;

        yield  return new WaitForSeconds(time); // таймер, через 10 секунд
        SceneManager.LoadScene(0); // выполнится эта строка
    }

    public void startParentControl()
    {
        StartCoroutine(Wait(Globals.time));
    }

    public void convertTime()
    {
        if (inputData.text == "")
        {
            inputData.text = "0";
        }

        Globals.time = Convert.ToInt32(inputData.text);
        Globals.time = Globals.time * 60;
    }
}
