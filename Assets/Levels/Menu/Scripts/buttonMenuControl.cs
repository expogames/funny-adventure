﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class buttonMenuControl : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setLevel1()
    {
        SceneManager.LoadScene(1);
    }
    public void setLevel2()
    {
        SceneManager.LoadScene(2);
    }
    public void setLevel3()
    {
        SceneManager.LoadScene(3);
    }
}
