﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playParentControlSystem : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (Globals.time != 0)
        {
            Globals.time -= Time.deltaTime;
            if (Globals.time < 0)
            {
                SceneManager.LoadScene(0);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Globals.time != 0)
        {
            Globals.time -= Time.deltaTime;
            if (Globals.time < 0)
            {
                SceneManager.LoadScene(0);
            }
        }
    }
}
