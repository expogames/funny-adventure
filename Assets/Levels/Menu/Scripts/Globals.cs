﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class Globals : MonoBehaviour {
    public  static float time;
    public static float soundVolume = 0.5f;
    public Slider slider;
    // Use this for initialization
    private void Start()
    {
        slider.value = Globals.soundVolume;
    }

    // Update is called once per frame
    void Update () {
        Globals.soundVolume = slider.value;
        AudioListener.volume = Globals.soundVolume;
    }
}
