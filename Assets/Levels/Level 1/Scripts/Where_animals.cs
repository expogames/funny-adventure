﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Where_animals : MonoBehaviour
{
    public GameObject lion;
    public GameObject monkey;
    public GameObject jiraf;
    public GameObject panelMenu;
    private int stars = starSystem.countStar;
    public Image star1;
    public Image star2;
    public Image star3;
    // Start is called before the first frame update
    void Start()
    {
        stars = 0;
        panelMenu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (stars == 3)
        {
            panelMenu.SetActive(true);
            star1.color = new Color(255, 255, 0f, 255);
            star2.color = new Color(255, 255, 0f, 255);
            star3.color = new Color(255, 255, 0f, 255);
            starSystem.countStar = stars;
        }
        if (stars == 1)
        {
            star1.color = new Color(255, 255, 0f, 255);
            starSystem.countStar = stars;
        }
        if (stars == 2)
        {
            star1.color = new Color(255, 255, 0f, 255);
            star2.color = new Color(255, 255, 0f, 255);
            starSystem.countStar = stars;
        }
    }

    public void btnLion()
    {
        if (lion.GetComponent<Animator>().enabled == false)
        {
            stars += 1;
        }
        lion.GetComponent<Animator>().enabled = true;
    }

    public void btnJiraf()
    {
        if (jiraf.GetComponent<Animator>().enabled == false)
        {
            stars += 1;
        }
        jiraf.GetComponent<Animator>().enabled = true;
    }

    public void btnMonkey()
    {
        if (monkey.GetComponent<Animator>().enabled == false)
        {
            stars += 1;
        }
        monkey.GetComponent<Animator>().enabled = true;
    }

    public void btnBack()
    {
        panelMenu.SetActive(true);
    }
}
